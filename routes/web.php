<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* API Document Information */
/* check http://project-url/api/documentation url to get more information about APIs*/
/* you can run "php artisan swagger-lume:generate" command to update document once you complete new APIs */
/* known bug - api doc will be broken if you have not configured virtual host for this project in local server i.e WAMP*/
/* still need help ? only god can help you :) just kidding . enjoy !!*/

$router->get('/', function () use ($router) {
    //return $router->app->version();
    return '<h1><marquee>Truck API</marquee></h1>';
});

//test push notification
$router->get('testingpush','ApiController@testPushNotification');

$router->group(['namespace' => 'API'], function() use ($router) {



    $router->group(['prefix' => 'auth'], function () use ($router) {
        $router->post('login','AuthController@login');
        $router->post('register','AuthController@register');
        $router->post('verifyMobile/{id}','AuthController@verifyMobile');
        $router->put('forgotPassword','AuthController@forgotPassword');
        $router->put('resetPassword','AuthController@resetPassword');
    });
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->put('{id}/updateProfile','UserController@updateProfile');
        $router->put('{id}/logout','UserController@logout');
        $router->put('{id}/sendCode','UserController@sendCode');
        $router->put('{id}/appStart','UserController@appStart');
    });

    /*********************** user app start ******************************/
    //truck list
    $router->get('trucks','TruckController@getTrucks');
	$router->get('trucks/{latitude}/{longitude}/{radius}','TruckController@getTrucks');
	$router->get('trucks/{area_id}','TruckController@getTrucks');
	$router->get('trucks/filter/{category_id}','TruckController@getTrucks');
    //catering list
    $router->get('caterings','CateringController@getCaterings');
    $router->get('caterings/{latitude}/{longitude}/{radius}','CateringController@getCaterings');
    $router->get('caterings/{area_id}','CateringController@getCaterings');
    $router->get('caterings/filter/{category_id}','CateringController@getCaterings');
	//offer list
    $router->get('offers/truck','OfferController@getOffers');
    $router->get('offers/truck/{latitude}/{longitude}/{radius}','OfferController@getOffers');
	
	$router->get('offers/catering','OfferController@getOffersCatering');
    $router->get('offers/catering/{latitude}/{longitude}/{radius}','OfferController@getOffersCatering');

    //order
    $router->post('user/{user_id}/order','OrderController@placeOrder');

    //review
    $router->post('user/{user_id}/{review_type}/{type_id}/review','ReviewController@addReview');
    /*********************** user app end ******************************/


    /*********************** restaurant app start ******************************/

    //manage truck
    $router->post('truck','TruckController@addTruck');
	$router->get('truck/{user_id}','TruckController@getTruckDetail');
    $router->post('updatetruck','TruckController@updateTruck');
    $router->get('user/{user_id}/truck-catering','TruckController@getTruck');

    // truck menu category
    $router->post('truck/{user_id}/menu/category','MenuCategoryController@addCategory');
    $router->post('truck/menu/category/{category_id}','MenuCategoryController@editCategory');
    $router->delete('truck/menu/category/{category_id}','MenuCategoryController@deleteCategory');

    // menu item
    $router->post('truck/menu','MenuItemController@addMenuItem');
    $router->post('truck/menu/{menu_id}','MenuItemController@editMenuItem');
    $router->delete('truck/menu/{menu_id}','MenuItemController@deleteMenuItem');
    $router->post('truck/menu/{menu_id}/attribute','MenuItemController@addMenuItemAttribute');
    $router->put('truck/menu/{menu_id}/attribute','MenuItemController@editMenuItemAttribute');
    $router->delete('truck/menu/{menu_id}/attribute','MenuItemController@deleteMenuItemAttribute');

    // catering
    $router->post('user/{user_id}/catering','CateringController@addCatering');
    $router->post('catering/{catering_id}','CateringController@editCatering');
    $router->delete('catering/{catering_id}','CateringController@deleteCatering');
    $router->post('catering/{catering_id}/menu','CateringController@addCateringMenu');
    $router->put('catering/{catering_id}/menu','CateringController@editCateringMenu');
    $router->delete('catering/{catering_id}/menu','CateringController@deleteCateringMenu');
	
	//offers
	$router->post('user/{user_id}/offer','OfferController@addOffer');
	$router->post('offer/{offer_id}','OfferController@editOffer');
	$router->delete('offer/{offer_id}','OfferController@deleteOffer');
	$router->get('offer/{offer_id}','OfferController@getOffer');
	
    //order
    $router->get('user/{user_id}/{user_type}/order','OrderController@getOrders');
    $router->put('order/{order_id}/updateStatus','OrderController@updateStatus');
	
	//area
	$router->get('areas','CityController@getAreas');
	
	//category
	$router->get('categories','CategoryController@getCategories');
    /*********************** restaurant app end ******************************/
});

