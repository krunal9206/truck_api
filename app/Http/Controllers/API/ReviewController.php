<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use \File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\User;
use App\Models\Truck;
use App\Models\Review;

class ReviewController extends ApiController
{

    /**
     * @SWG\Post(
     *     path="/user/{user_id}/{review_type}/{type_id}/review",
     *     summary="This api is used to add review",
     *     tags={"User App"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id of user who is adding review",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="review_type",
     *         in="path",
     *         description="Value should Truck or Catering",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="path",
     *         description="truck id or catering id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="star",
     *         in="formData",
     *         description="given star by user like 1/2/3/4/5",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="comment",
     *         in="formData",
     *         description="comment of review",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addReview(Request $request,$userId,$type,$typeId)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'star' => 'required',
            'comment' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $user = $user->find($userId);
        if (!$user) {
            return $this->response(null, 'f', '500', 'user id is invalid');
        }
        $truck = new Truck();
        $user = $truck->find($typeId);
        if (!$user) {
            return $this->response(null, 'f', '500', 'type id is invalid');
        }
        $reviewType = ['Truck','Catering'];
        if(!in_array($type,$reviewType)){
            return $this->response(null, 'f', '500', 'type is invalid. must be Truck or Catering');
        }

        $request->request->add(['user_id' => $userId]);
        $request->request->add(['type' => $type]);
        $request->request->add(['type_id' => $typeId]);
        $review = new Review();
        $save = $review->create($request->all());



        if ($save) {
            $review = new Review();
            $get = $review::with('user')->where('id',$save->id)->get();
            return $this->response($get, 's', '200', 'review has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add review');


    }


}
