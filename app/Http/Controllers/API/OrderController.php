<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Models\CartAttribute;
use App\Models\Catering;
use App\Models\CateringMenu;
use App\Models\Truck;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use \File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\Order;
use App\Models\User;
use App\Models\Cart;
use DB;


class OrderController extends ApiController
{
    public function __construct(Request $request)
    {

        $this->request = $request;

    }

    /**
     * @SWG\Get(
     *     path="/user/{user_id}/{user_type}/order",
     *     summary="This api is used get order list",
     *     tags={"User App","Restaurant App"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="user_type",
     *         in="path",
     *         description="user type - user or restaurant",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getOrders(Request $request, $id, $type)
    {
        $order = new Order();
        $orderData = $order->getOrders($id, $type);

        if ($orderData) {
            return $this->response($orderData, 's', '200', '');
        }
        return $this->response($orderData, 'f', '500', 'no order found');
    }

    /**
     * @SWG\Post(
     *     path="/user/{user_id}/order",
     *     summary="This api is used place order",
     *     tags={"User App"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string",
     *     ),
     * @SWG\Parameter(
     *   name="order",
     *   in="body",
     *   description="order object",
     *   required=true,
     *   @SWG\Schema(
     *       type="object",
     *       @SWG\Property(property="order_type", type="string"),
     *       @SWG\Property(property="restaurant_id", type="integer"),
     *       @SWG\Property(property="payment_type", type="string"),
     *       @SWG\Property(property="delivery_date", type="string"),
     *       @SWG\Property(property="delivery_time", type="string"),
     *       @SWG\Property(property="address_id", type="integer"),
     *       @SWG\Property(property="user_address_lat", type="string"),
     *       @SWG\Property(property="user_address_lng", type="string"),
     *       @SWG\Property(property="user_address_block", type="string"),
     *       @SWG\Property(property="user_address_street", type="string"),
     *       @SWG\Property(property="user_address_home_no", type="string"),
     *       @SWG\Property(property="comment", type="string"),
     *       @SWG\Property(property="special_instruction", type="string"),
     *       @SWG\Property(property="female_service", type="boolean"),
     *           @SWG\Property(
     *               property="cart",
     *               type="array",
     *               @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="menu_id", type="integer"),
     *                  @SWG\Property(property="menu_title", type="string"),
     *                  @SWG\Property(property="menu_image", type="string"),
     *                  @SWG\Property(property="price", type="string"),
     *                  @SWG\Property(property="quantity", type="string"),
     *     @SWG\Property(
     *               property="attribute",
     *               type="array",
     *               @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="attribute_id", type="integer"),
     *                  @SWG\Property(property="attribute_name", type="string"),
     *                  @SWG\Property(property="value", type="string"),
     *                  @SWG\Property(property="price", type="string")
     *         )
     *       )
     *         )
     *       )
     *     )
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function placeOrder(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'order_type' => 'required|in:Truck,Catering',
            'restaurant_id'=>'required',
            'payment_type' => 'required|in:Cash on Delivery,Credit Card,Pick up order',
            'delivery_date' => 'required',
            'delivery_time' => 'required',
            'address_id' => 'required',
            'user_address_lat' => 'required',
            'user_address_lng' => 'required',
            'user_address_block' => 'required',
            'user_address_street' => 'required',
            'user_address_home_no' => 'required',
            'comment' => 'required',
			'female_service' => 'boolean',
            'cart' => 'required|array',
            'cart.0.menu_id' => 'required',
            'cart.0.menu_title' => 'required',
            'cart.0.menu_image' => 'required',
            'cart.0.price' => 'required',
            'cart.0.quantity' => 'required',
            //'cart.0.attribute' => 'sometimes|required|array'

        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        $user = new User();
        $userData = $user::find($id);
        if ($userData) {


            $arr = $request->input();


            $order = new Order();
            $order->user_id = $id;
            $order->order_type = $arr['order_type'];
            $order->restaurant_id = $arr['restaurant_id'];
            $order->payment_type = $arr['payment_type'];
            $order->address_id = $arr['address_id'];
            $order->delivery_date = $arr['delivery_date'];
            $order->delivery_time = $arr['delivery_time'];
            $order->user_address_lat = $arr['user_address_lat'];
            $order->user_address_lng = $arr['user_address_lng'];
            $order->user_address_block = $arr['user_address_block'];
            $order->user_address_street = $arr['user_address_street'];
            $order->user_address_home_no = $arr['user_address_home_no'];
            $order->comment = $arr['comment'];
			if($arr['order_type'] == 'Catering'){
				$order->special_instruction = $arr['special_instruction'];
				$order->female_service = $arr['female_service'];
			}
            $order->save();

            foreach ($arr['cart'] as $a) {
                $cart = new Cart();

                $cart->menu_id = $a['menu_id'];
                $cart->menu_title = $a['menu_title'];
                $cart->menu_image = $a['menu_image'];
                $cart->price = $a['price'];
                $cart->quantity = $a['quantity'];

                $order->cart()->save($cart);
				if(isset($a['attribute'])){
					foreach ($a['attribute'] as $attr) {
						$attribute = new CartAttribute();

						$attribute->attribute_id = $attr['attribute_id'];
						$attribute->attribute_name = $attr['attribute_name'];
						$cart->value = $attr['value'];
						$attribute->price = $attr['price'];
						$cart->attribute()->save($attribute);
					}
				}


            }


            $orderId = $order->id;
            $order = new Order();
            $get = $order::with(['cart.attribute'])->where('id', $orderId)->get();

            //send push notification
            /* $truck = new Truck();
            $getOwner = $truck->getOwner($arr['restaurant_id']);

            $user = new User();
            $pushData = $user->getPushData($getOwner->user_id);

            $data = ['order_id' => $orderId];
			if(isset($pushData->device_token)){
				$this->sendPushNotification($pushData->device_token, '', 'Received New Order', $data, 'Order');
			} */
            return $this->response($get, 's', '200', 'order has been placed');
        } else {
            return $this->response(null, 'f', '404', 'user id is invalid');
        }
    }
    /**
     * @SWG\Put(
     *     path="/order/{order_id}/updateStatus",
     *     summary="This api is used to update order status",
     *     tags={"Restaurant App","User App"},
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="order id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         in="formData",
     *         description="status of order",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function updateStatus(Request $request, $id){

        $order = new Order();
        $orderData = $order->find($id);
        if(!$orderData) {
            return $this->response(null,'f','500','order not found');
        }

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:Confirmed,Processing,Cancelled,Delivered,Returned,Completed'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        //update order fields
        $update =$order->find($id)->update(['status'=>$request->input('status')]);

        if($update){
            /* $user = new User();
            $pushData = $user->getPushData($orderData->user_id);


            $data = ['order_id' => $id];
            $this->sendPushNotification($pushData->device_token, '', 'Order is '.$request->input('status'), $data, 'Order'); */

            $orderData = $order->find($id);
            return $this->response($orderData,'s','200','order status has been changed successfully');
        }
        return $this->response(null,'f','500','failed to update order');
    }


}
