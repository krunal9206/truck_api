<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Category;


class CategoryController extends ApiController
{
    /**
     * @SWG\Get(
     *     path="/categories",
     *     summary="This api is used to get categories list",
     *     tags={"Restaurant App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getCategories()
    {
        $categories = Category::get();

        if ($categories) {
            return $this->response($categories, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to category area list');
    }
}
