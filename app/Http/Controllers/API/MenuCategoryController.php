<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Models\MenuCategory;
use App\Models\Truck;
use App\Models\User;

class MenuCategoryController extends ApiController
{
    public function __construct(Request $request)
    {

        $this->request = $request;

    }

    /**
     * @SWG\Post(
     *     path="/truck/{user_id}/menu/category",
     *     summary="This api is used to add truck menu category",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id of user who is adding truck category",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of truck menu category",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of truck menu category",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu category",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addCategory(Request $request, $id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'sub_title' => 'required',
            'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $user = $user->find($id);
        if (!$user) {
            return $this->response(null, 'f', '500', 'user id is invalid');
        }
        $truck = new Truck();
        $get = $truck::where('user_id', $id)->first();

        $image = $request->file('main_image');

        $filename = $image->hashName();

        $originalPath = base_path('public/uploads/images/truck/menu/category/original/' . $filename);
        $thumbPath = base_path('public/uploads/images/truck/menu/category/thumb/' . $filename);
        $image_resize = Image::make($image->getRealPath());


        //save original file
        $image_resize->save($originalPath);

        $image_resize->resize(380, 366, function ($constraint) {
            //$constraint->aspectRatio();
        });

        //save resize file
        $imageSave = $image_resize->save($thumbPath);
        //set image url
        $imageUrl = url(Storage::disk('truck_category')->url($filename));

        if (!$imageSave)
            return $this->response(null, 'f', '500', 'failed to add menu category image');


        $request->request->add(['image' => $imageUrl]);

        if ($get) {

            $request->request->add(['truck_id' => $get->id]);
            $menuCategory = new MenuCategory();
            $save = $menuCategory->create($request->all());

        } else {
            return $this->response(null, 'f', '500', 'Please fill out your profile items first');

        }

        if ($save) {
            return $this->response($save, 's', '200', 'menu category has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add menu category detail');


    }

    /**
     * @SWG\Post(
     *     path="/truck/menu/category/{category_id}",
     *     summary="This api is used to edit truck menu category",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="category_id",
     *         in="path",
     *         description="category id of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of truck menu category",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of truck menu category",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu category",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editCategory(Request $request, $id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'sub_title' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $menu = new MenuCategory();
        $menuCat = $menu->find($id);
        if (!$menuCat) {
            return $this->response(null, 'f', '500', 'category id is invalid');
        }


        $image = $request->file('main_image');
        if($image){
            $validator = Validator::make($request->all(), [
                'main_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }

            $filename = $image->hashName();

            //remove old image if new image uploaded on update truck
            $deleteImageName = pathinfo($menuCat->image)['basename'];
            unlink('uploads/images/truck/menu/category/original/'.$deleteImageName);
            unlink('uploads/images/truck/menu/category/thumb/'.$deleteImageName);

            $originalPath = base_path('public/uploads/images/truck/menu/category/original/' . $filename);
            $thumbPath = base_path('public/uploads/images/truck/menu/category/thumb/' . $filename);
            $image_resize = Image::make($image->getRealPath());


            //save original file
            $image_resize->save($originalPath);

            $image_resize->resize(380, 366, function ($constraint) {
                //$constraint->aspectRatio();
            });

            //save resize file
            $imageSave = $image_resize->save($thumbPath);
            //set image url
            $imageUrl = url(Storage::disk('truck_category')->url($filename));

            if (!$imageSave)
                return $this->response(null, 'f', '500', 'failed to add menu category image');
        }else{
            $imageUrl = $menuCat->image;
        }

        $request->request->add(['image' => $imageUrl]);
        $menuCategory = new MenuCategory();

        $save = $menuCategory::find($id)->update($request->only(['title','sub_title','image']));
        $save = $menuCategory::find($id);

        if ($save) {
            return $this->response($save, 's', '200', 'menu category has been edited successfully');
        }
        return $this->response(null, 'f', '500', 'failed to edit menu category detail');


    }
    /**
     * @SWG\Delete(
     *     path="/truck/menu/category/{category_id}",
     *     summary="This api is used delete truck menu category",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="category_id",
     *         in="path",
     *         description="category id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteCategory($id)
    {
        $menuCategory = new MenuCategory();
        $menuCat = $menuCategory::find($id);
        if ($menuCat) {

            $delete = $menuCategory::find($id)->delete();
            if($delete){
                //remove old image if new image uploaded on update truck
                $deleteImageName = pathinfo($menuCat->image)['basename'];
                unlink('uploads/images/truck/menu/category/original/'.$deleteImageName);
                unlink('uploads/images/truck/menu/category/thumb/'.$deleteImageName);

                return $this->response(null, 's', '200', 'truck menu category has been deleted');
            }
            return $this->response(null, 'f', '500', 'failed to delete truck menu category');

        } else {
            return $this->response(null, 'f', '404', 'category id is invalid');
        }


    }

}
