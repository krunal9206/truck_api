<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Models\MenuCategory;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Models\MenuItem;
use App\Models\MenuItemAttribute;
use App\Models\MenuItemAttributeValue;
use App\Models\Truck;


class MenuItemController extends ApiController
{

    /**
     * @SWG\Post(
     *     path="/truck/menu",
     *     summary="This api is used to add menu of truck category",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="category_id",
     *         in="formData",
     *         description="category id of menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of truck menu ",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="price",
     *         in="formData",
     *         description="price of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addMenuItem(Request $request)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
            'sub_title' => 'required',
            'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'price' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $menuCat = new MenuCategory();
        $menuCat = $menuCat->find($request->input('category_id'));
        if (!$menuCat) {
            return $this->response(null, 'f', '500', 'category not found');
        }

        $image = $request->file('main_image');

        $filename = $image->hashName();

        $originalPath = base_path('public/uploads/images/truck/menu/original/' . $filename);
        $thumbPath = base_path('public/uploads/images/truck/menu/thumb/' . $filename);
        $image_resize = Image::make($image->getRealPath());


        //save original file
        $image_resize->save($originalPath);

        $image_resize->resize(380, 366, function ($constraint) {
            //$constraint->aspectRatio();
        });

        //save resize file
        $imageSave = $image_resize->save($thumbPath);
        //set image url
        $imageUrl = url(Storage::disk('truck_menu')->url($filename));

        if (!$imageSave)
            return $this->response(null, 'f', '500', 'failed to add menu image');


        $request->request->add(['image' => $imageUrl]);
        $menuItem = new MenuItem();
        $saveMenuItem = $menuItem->create($request->all());
        if ($saveMenuItem) {
            return $this->response($saveMenuItem, 's', '200', 'menu has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add menu ');


    }
    /**
     * @SWG\Post(
     *     path="/truck/menu/{menu_id}",
     *     summary="This api is used to edit menu of truck category",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="menu_id",
     *         in="path",
     *         description="menu id of menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of truck menu ",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="price",
     *         in="formData",
     *         description="price of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editMenuItem(Request $request, $id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'sub_title' => 'required',
            'price' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $menu = new MenuItem();
        $menuItem = $menu->find($id);
        if (!$menuItem) {
            return $this->response(null, 'f', '500', 'menu id is invalid');
        }


        $image = $request->file('main_image');
        if($image){
            $validator = Validator::make($request->all(), [
                'main_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }

            $filename = $image->hashName();

            //remove old image if new image uploaded on update truck
            $deleteImageName = pathinfo($menuItem->image)['basename'];
            unlink('uploads/images/truck/menu/original/'.$deleteImageName);
            unlink('uploads/images/truck/menu/thumb/'.$deleteImageName);

            $originalPath = base_path('public/uploads/images/truck/menu/original/' . $filename);
            $thumbPath = base_path('public/uploads/images/truck/menu/thumb/' . $filename);
            $image_resize = Image::make($image->getRealPath());


            //save original file
            $image_resize->save($originalPath);

            $image_resize->resize(380, 366, function ($constraint) {
                //$constraint->aspectRatio();
            });

            //save resize file
            $imageSave = $image_resize->save($thumbPath);
            //set image url
            $imageUrl = url(Storage::disk('truck_menu')->url($filename));

            if (!$imageSave)
                return $this->response(null, 'f', '500', 'failed to add menu category image');
        }else{
            $imageUrl = $menuItem->image;
        }

        $request->request->add(['image' => $imageUrl]);
        $menu = new MenuItem();

        $save = $menu::find($id)->update($request->only(['title','sub_title','image','price']));
        $save = $menu::find($id);

        if ($save) {
            return $this->response($save, 's', '200', 'menu has been edited successfully');
        }
        return $this->response(null, 'f', '500', 'failed to edit menu detail');


    }
    /**
     * @SWG\Delete(
     *     path="/truck/menu/{menu_id}",
     *     summary="This api is used delete truck menu",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="menu_id",
     *         in="path",
     *         description="menu id of menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteMenuItem($id)
    {
        $menu = new MenuItem();
        $menuIteam = $menu::find($id);
        if ($menuIteam) {

            $delete = $menuIteam::find($id)->delete();
            if($delete){
                //remove old image if new image uploaded on update truck
                $deleteImageName = pathinfo($menuIteam->image)['basename'];
                unlink('uploads/images/truck/menu/original/'.$deleteImageName);
                unlink('uploads/images/truck/menu/thumb/'.$deleteImageName);

                return $this->deleteMenuItemAttribute($id);

            }
            return $this->response(null, 'f', '500', 'failed to delete menu');

        } else {
            return $this->response(null, 'f', '404', 'menu id is invalid');
        }


    }
    /**
     * @SWG\Post(
     *     path="/truck/menu/{menu_id}/attribute",
     *     summary="This api is used add truck menu attribute",
     *     tags={"Restaurant App"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="menu_id",
     *         in="path",
     *         description="menu id of menu",
     *         required=true,
     *         type="string",
     *     ),
     * @SWG\Parameter(
     *   name="attribute_object",
     *   in="body",
     *   description="attribute object",
     *   required=true,
     *   @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="menu_item_attribute",
     *         type="array",
     *         @SWG\Items(
     *           type="object",
     *           @SWG\Property(property="title", type="string"),
     *           @SWG\Property(property="type", type="string"),
     *           @SWG\Property(
     *               property="menu_item_attribute_value",
     *               type="array",
     *               @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="value", type="string"),
     *                  @SWG\Property(property="price", type="string")
     *         )
     *       )
     *         )
     *       )
     *     )
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addMenuItemAttribute(Request $request,$id){


        // Perform Validation
        $validator = Validator::make($request->all(), [
            'menu_item_attribute' => 'required|array',
            'menu_item_attribute.0.title' => 'required',
            'menu_item_attribute.0.type' => 'required',
            'menu_item_attribute.0.menu_item_attribute_value' => 'required|array',
            'menu_item_attribute.0.menu_item_attribute_value.0.value' => 'required',
            'menu_item_attribute.0.menu_item_attribute_value.0.price' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        $menu = new MenuItem();
        $menuIteam = $menu::find($id);
        if ($menuIteam) {
            $array = $request->input('menu_item_attribute');
            foreach($array as $arr){

                $menuItemAttribute = new MenuItemAttribute();
                $menuItemAttribute->menu_item_id=$id;
                $menuItemAttribute->title=$arr['title'];
                $menuItemAttribute->type=$arr['type'];
                $menuItemAttribute->save();

                foreach($arr['menu_item_attribute_value'] as $a){
                    $menuItemAttributeValue = new MenuItemAttributeValue();
                    $menuItemAttributeValue->value=$a['value'];
                    $menuItemAttributeValue->price=$a['price'];
                    $menuItemAttribute->menuItemAttributeValue()->save($menuItemAttributeValue);
                }

            }
            $get = $menuItemAttribute::with(['menuItemAttributeValue'])->where('menu_item_id',$id)->get();
            return $this->response($get, 's', '200', 'menu attribute has been added');
        } else {
            return $this->response(null, 'f', '404', 'menu id is invalid');
        }
    }
    /**
     * @SWG\Put(
     *     path="/truck/menu/{menu_id}/attribute",
     *     summary="This api is used edit truck menu attribute",
     *     tags={"Restaurant App"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="menu_id",
     *         in="path",
     *         description="menu id of menu",
     *         required=true,
     *         type="string",
     *     ),
     * @SWG\Parameter(
     *   name="attribute_object",
     *   in="body",
     *   description="attribute object",
     *   required=true,
     *   @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="menu_item_attribute",
     *         type="array",
     *         @SWG\Items(
     *           type="object",
     *           @SWG\Property(property="title", type="string"),
     *           @SWG\Property(property="type", type="string"),
     *           @SWG\Property(
     *               property="menu_item_attribute_value",
     *               type="array",
     *               @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="value", type="string"),
     *                  @SWG\Property(property="price", type="string")
     *         )
     *       )
     *         )
     *       )
     *     )
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editMenuItemAttribute(Request $request,$id){
        // Perform Validation
        $validator = Validator::make($request->all(), [
            'menu_item_attribute' => 'required|array',
            'menu_item_attribute.0.title' => 'required',
            'menu_item_attribute.0.type' => 'required',
            'menu_item_attribute.0.menu_item_attribute_value' => 'required|array',
            'menu_item_attribute.0.menu_item_attribute_value.0.value' => 'required',
            'menu_item_attribute.0.menu_item_attribute_value.0.price' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        $menu = new MenuItem();
        $menuItem = $menu::find($id);
        if ($menuItem) {

            //remove old record first before adding new one
            $menuItemAttribute = new MenuItemAttribute();
            $menuItemAttributeValue = new MenuItemAttributeValue();
            $ids = $menuItemAttribute::all()->where('menu_item_id',$id)->pluck('id');
            if(count($ids) > 0){
                foreach($ids as $d){
                    $menuItemAttribute::find($d)->delete();
                    $menuItemAttributeValue->where('attribute_id', $d)->delete();
                    }
            }

            $array = $request->input('menu_item_attribute');
            foreach($array as $arr){

                $menuItemAttribute = new MenuItemAttribute();
                $menuItemAttribute->menu_item_id=$id;
                $menuItemAttribute->title=$arr['title'];
                $menuItemAttribute->type=$arr['type'];
                $menuItemAttribute->save();

                foreach($arr['menu_item_attribute_value'] as $a){
                    $menuItemAttributeValue = new MenuItemAttributeValue();
                    $menuItemAttributeValue->value=$a['value'];
                    $menuItemAttributeValue->price=$a['price'];
                    $menuItemAttribute->menuItemAttributeValue()->save($menuItemAttributeValue);
                }

            }
            $get = $menuItemAttribute::with(['menuItemAttributeValue'])->where('menu_item_id',$id)->get();
            return $this->response($get, 's', '200', 'menu attribute has been edited');
        } else {
            return $this->response(null, 'f', '404', 'menu id is invalid');
        }
    }
    /**
     * @SWG\Delete(
     *     path="/truck/menu/{menu_id}/attribute",
     *     summary="This api is used delete menu attribute",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="menu_id",
     *         in="path",
     *         description="menu id of menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteMenuItemAttribute($id)
    {

            //remove old record first before adding new one
            $menuItemAttribute = new MenuItemAttribute();
            $menuItemAttributeValue = new MenuItemAttributeValue();
            $ids = $menuItemAttribute::all()->where('menu_item_id',$id)->pluck('id');
            if(count($ids) > 0){
                foreach($ids as $d){
                    $menuItemAttribute::find($d)->delete();
                    $menuItemAttributeValue->where('attribute_id', $d)->delete();
                }
            }

            return $this->response(null, 's', '200', 'item has been deleted');




    }


}
