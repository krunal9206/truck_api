<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Models\Offer;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use \File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\User;
use DB;


class OfferController extends ApiController
{
    public function __construct(Request $request)
    {

        $this->request = $request;

    }

	/**
     * @SWG\Get(
     *     path="/offers/truck",
     *     summary="This api is used get offer list for truck",
     *     tags={"User App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/offers/truck/{latitude}/{longitude}/{radius}",
     *     summary="This api is used get offer list by latitude, longitude and radius for truck",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="latitude",
     *         in="path",
     *         description="latitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="longitude",
     *         in="path",
     *         description="longitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="radius",
     *         in="path",
     *         description="radius",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getOffers($latitude = '', $longitude = '', $radius = '')
    {
        $user = new User();
        $detail = $user->getOffers($latitude, $longitude, $radius);

        if ($detail) {
            return $this->response($detail, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to get truck list');
    }
	
	/**
     * @SWG\Get(
     *     path="/offers/catering",
     *     summary="This api is used get offer list for catering",
     *     tags={"User App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/offers/catering/{latitude}/{longitude}/{radius}",
     *     summary="This api is used get offer list by latitude, longitude and radius for catering",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="latitude",
     *         in="path",
     *         description="latitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="longitude",
     *         in="path",
     *         description="longitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="radius",
     *         in="path",
     *         description="radius",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getOffersCatering($latitude = '', $longitude = '', $radius = '')
    {
        $user = new User();
        $detail = $user->getOffersCatering($latitude, $longitude, $radius);

        if ($detail) {
            return $this->response($detail, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to get truck list');
    }

    /**
     * @SWG\Post(
     *     path="/user/{user_id}/offer",
     *     summary="This api is used to add offer",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of offer",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="description of offer",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of offer",
     *         required=true,
     *         type="file"
     *     ),
	 *      @SWG\Parameter(
     *         name="offer_percentage",
     *         in="formData",
     *         description="offer_percentage of offer",
     *         required=false,
     *         type="string"
     *     ),
	 *      @SWG\Parameter(
     *         name="is_catering",
     *         in="formData",
     *         description="1 for catering / 0 for truck",
     *         required=true,
     *         type="string"
     *     ),
	  *      @SWG\Parameter(
     *         name="is_active",
     *         in="formData",
     *         description="1 for active / 0 for inactive",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addOffer(Request $request,$id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'is_catering' => 'required',
            'is_active' => 'required',
            'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $userDet = $user->find($id);
        if (!$userDet) {
            return $this->response(null, 'f', '500', 'user not found');
        }

        $image = $request->file('main_image');

        $filename = $image->hashName();

        $originalPath = base_path('public/uploads/images/offer/original/' . $filename);
        $thumbPath = base_path('public/uploads/images/offer/thumb/' . $filename);
        $image_resize = Image::make($image->getRealPath());


        //save original file
        $image_resize->save($originalPath);

        $image_resize->resize(380, 366, function ($constraint) {
            //$constraint->aspectRatio();
        });

        //save resize file
        $imageSave = $image_resize->save($thumbPath);
        //set image url
        $imageUrl = url(Storage::disk('offer')->url($filename));

        if (!$imageSave)
            return $this->response(null, 'f', '500', 'failed to add offer image');

        $request->request->add(['user_id' => $id]);
        $request->request->add(['image' => $imageUrl]);
        $offer = new Offer();
        $saveOffer = $offer->create($request->all());
        if ($saveOffer) {
            return $this->response($saveOffer, 's', '200', 'offer has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add offer ');


    }
    /**
     * @SWG\Post(
     *     path="/offer/{offer_id}",
     *     summary="This api is used to edit offer",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="offer_id",
     *         in="path",
     *         description="offer id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of offer",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="description of offer",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of offer",
     *         required=false,
     *         type="file"
     *     ),
	 *      @SWG\Parameter(
     *         name="offer_percentage",
     *         in="formData",
     *         description="offer_percentage of offer",
     *         required=false,
     *         type="string"
     *     ),
	 *      @SWG\Parameter(
     *         name="is_catering",
     *         in="formData",
     *         description="1 for catering / 0 for truck",
     *         required=true,
     *         type="string"
     *     ),
	 *      @SWG\Parameter(
     *         name="is_active",
     *         in="formData",
     *         description="1 for active / 0 for inactive",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editOffer(Request $request, $id)
    {
        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
			'is_catering' => 'required',
            'is_active' => 'required',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $offer = new Offer();
        $offerData = $offer->find($id);
        if (!$offerData) {
            return $this->response(null, 'f', '500', 'offer id is invalid');
        }


        $image = $request->file('main_image');
        if($image){
            $validator = Validator::make($request->all(), [
                'main_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }

            $filename = $image->hashName();

            //remove old image if new image uploaded on update truck
            $deleteImageName = pathinfo($offerData->image)['basename'];
            unlink('uploads/images/offer/original/'.$deleteImageName);
            unlink('uploads/images/offer/thumb/'.$deleteImageName);

            $originalPath = base_path('public/uploads/images/offer/original/' . $filename);
            $thumbPath = base_path('public/uploads/images/offer/thumb/' . $filename);
            $image_resize = Image::make($image->getRealPath());


            //save original file
            $image_resize->save($originalPath);

            $image_resize->resize(380, 366, function ($constraint) {
                //$constraint->aspectRatio();
            });

            //save resize file
            $imageSave = $image_resize->save($thumbPath);
            //set image url
            $imageUrl = url(Storage::disk('offer')->url($filename));

            if (!$imageSave)
                return $this->response(null, 'f', '500', 'failed to add offer image');
        }else{
            $imageUrl = $offerData->image;
        }

        $request->request->add(['image' => $imageUrl]);
        $offer = new Offer();

		$save = $offer::find($id)->update($request->only(['title','description','image','offer_percentage','is_active','is_catering']));
        $save = $offer::find($id);

        if ($save) {
            return $this->response($save, 's', '200', 'offer has been edited successfully');
        }
        return $this->response(null, 'f', '500', 'failed to edit offer detail');


    }
	/**
     * @SWG\Delete(
     *     path="/offer/{offer_id}",
     *     summary="This api is used delete offer",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="offer_id",
     *         in="path",
     *         description="offer id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteOffer($id)
    {
        $offer = new Offer();
        $offerData = $offer::find($id);
        if ($offerData) {

            $delete = $offerData::find($id)->delete();
            if($delete){
                //remove old image if new image uploaded on update truck
                $deleteImageName = pathinfo($offerData->image)['basename'];
                unlink('uploads/images/offer/original/'.$deleteImageName);
                unlink('uploads/images/offer/thumb/'.$deleteImageName);

                return $this->response(null, 's', '200', 'offer has been deleted');

            }
            return $this->response(null, 'f', '500', 'failed to delete offer');

        } else {
            return $this->response(null, 'f', '404', 'offer id is invalid');
        }


    }
	
	/**
     * @SWG\Get(
     *     path="/offer/{id}",
     *     summary="This api is used get offer by id",
     *     tags={"Restaurant App"},
	 *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="offer id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	
	public function getOffer($id)
    {
		$offer = new Offer();
        $offerData = $offer::find($id);
		
        if ($offerData) {
            return $this->response($offerData, 's', '200', 'offer has been found successfully');
        }
        return $this->response(null, 'f', '500', 'failed to get offer ');
    }
}
