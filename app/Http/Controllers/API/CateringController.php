<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Models\Catering;
use App\Models\CateringMenu;
use App\Models\Truck;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use \File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\User;
use DB;


class CateringController extends ApiController
{
    public function __construct(Request $request)
    {

        $this->request = $request;

    }

    /**
     * @SWG\Get(
     *     path="/caterings",
     *     summary="This api is used get catering list",
     *     tags={"User App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/caterings/{area_id}",
     *     summary="This api is used get catering list by area id",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="area_id",
     *         in="path",
     *         description="area_id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/caterings/filter/{category_id}",
     *     summary="This api is used get catering list by category id",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="category_id",
     *         in="path",
     *         description="category id (ex. 1,2,3)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/caterings/{latitude}/{longitude}/{radius}",
     *     summary="This api is used get catering list by latitude, longitude and radius",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="latitude",
     *         in="path",
     *         description="latitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="longitude",
     *         in="path",
     *         description="longitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="radius",
     *         in="path",
     *         description="radius",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getCaterings($latitude = '', $longitude = '', $radius = '', $area_id = '', $category_id = '')
    {
        $user = new User();
        $detail = $user->getCaterings($latitude, $longitude, $radius, $area_id, $category_id);

        if ($detail) {
            return $this->response($detail, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to get truck list');
    }

    /**
     * @SWG\Post(
     *     path="/user/{user_id}/catering",
     *     summary="This api is used to add catering",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of catering",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of catering",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="price",
     *         in="formData",
     *         description="price of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="serve_people",
     *         in="formData",
     *         description="serve people like 5-10 Person",
     *         required=true,
     *         type="string"
     *     ), 
	 *     @SWG\Parameter(
     *         name="setup_time",
     *         in="formData",
     *         description="Setup Time",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="max_time",
     *         in="formData",
     *         description="Max Time",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="service_presentation",
     *         in="formData",
     *         description="Service Presentation",
     *         required=true,
     *         type="string"
     *     ), 
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addCatering(Request $request,$id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'sub_title' => 'required',
            'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'price' => 'required',
            'serve_people' => 'required',
            'setup_time' => 'required',
            'max_time' => 'required',
            'service_presentation' => 'required',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $userDet = $user->find($id);
        if (!$userDet) {
            return $this->response(null, 'f', '500', 'user not found');
        }

        $image = $request->file('main_image');

        $filename = $image->hashName();

        $originalPath = base_path('public/uploads/images/catering/original/' . $filename);
        $thumbPath = base_path('public/uploads/images/catering/thumb/' . $filename);
        $image_resize = Image::make($image->getRealPath());


        //save original file
        $image_resize->save($originalPath);

        $image_resize->resize(380, 366, function ($constraint) {
            //$constraint->aspectRatio();
        });

        //save resize file
        $imageSave = $image_resize->save($thumbPath);
        //set image url
        $imageUrl = url(Storage::disk('catering')->url($filename));

        if (!$imageSave)
            return $this->response(null, 'f', '500', 'failed to add catering image');

        $request->request->add(['user_id' => $id]);
        $request->request->add(['image' => $imageUrl]);
        $catering = new Catering();
        $saveCatering = $catering->create($request->all());
        if ($saveCatering) {
            return $this->response($saveCatering, 's', '200', 'catering has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add catering ');


    }
    /**
     * @SWG\Post(
     *     path="/catering/{catering_id}",
     *     summary="This api is used to edit catering",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="catering_id",
     *         in="path",
     *         description="catering id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of catering",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of catering",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="price",
     *         in="formData",
     *         description="price of truck menu",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="serve_people",
     *         in="formData",
     *         description="serve people like 5-10 Person",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="setup_time",
     *         in="formData",
     *         description="Setup Time",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="max_time",
     *         in="formData",
     *         description="Max Time",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="service_presentation",
     *         in="formData",
     *         description="Service Presentation",
     *         required=true,
     *         type="string"
     *     ), 
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck menu",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editCatering(Request $request, $id)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'sub_title' => 'required',
            'price' => 'required',
            'serve_people' => 'required',
			'setup_time' => 'required',
            'max_time' => 'required',
            'service_presentation' => 'required',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $catering = new Catering();
        $cateringData = $catering->find($id);
        if (!$cateringData) {
            return $this->response(null, 'f', '500', 'catering id is invalid');
        }


        $image = $request->file('main_image');
        if($image){
            $validator = Validator::make($request->all(), [
                'main_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }

            $filename = $image->hashName();

            //remove old image if new image uploaded on update truck
            $deleteImageName = pathinfo($cateringData->image)['basename'];
            unlink('uploads/images/catering/original/'.$deleteImageName);
            unlink('uploads/images/catering/thumb/'.$deleteImageName);

            $originalPath = base_path('public/uploads/images/catering/original/' . $filename);
            $thumbPath = base_path('public/uploads/images/catering/thumb/' . $filename);
            $image_resize = Image::make($image->getRealPath());


            //save original file
            $image_resize->save($originalPath);

            $image_resize->resize(380, 366, function ($constraint) {
                //$constraint->aspectRatio();
            });

            //save resize file
            $imageSave = $image_resize->save($thumbPath);
            //set image url
            $imageUrl = url(Storage::disk('catering')->url($filename));

            if (!$imageSave)
                return $this->response(null, 'f', '500', 'failed to add catering image');
        }else{
            $imageUrl = $cateringData->image;
        }

        $request->request->add(['image' => $imageUrl]);
        $catering = new Catering();

        $save = $catering::find($id)->update($request->only(['title','sub_title','image','price','serve_people', 'setup_time', 'max_time', 'service_presentation']));
        $save = $catering::find($id);

        if ($save) {
            return $this->response($save, 's', '200', 'catering has been edited successfully');
        }
        return $this->response(null, 'f', '500', 'failed to edit catering detail');


    }
    /**
     * @SWG\Delete(
     *     path="/catering/{catering_id}",
     *     summary="This api is used delete catering",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="catering_id",
     *         in="path",
     *         description="catering id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteCatering($id)
    {
        $catering = new Catering();
        $cateringData = $catering::find($id);
        if ($cateringData) {

            $delete = $cateringData::find($id)->delete();
            if($delete){
                //remove old image if new image uploaded on update truck
                $deleteImageName = pathinfo($cateringData->image)['basename'];
                unlink('uploads/images/catering/original/'.$deleteImageName);
                unlink('uploads/images/catering/thumb/'.$deleteImageName);

                return $this->deleteCateringMenu($id);

            }
            return $this->response(null, 'f', '500', 'failed to delete catering');

        } else {
            return $this->response(null, 'f', '404', 'catering id is invalid');
        }


    }
    /**
     * @SWG\Post(
     *     path="/catering/{catering_id}/menu",
     *     summary="This api is used add catering menu",
     *     tags={"Restaurant App"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="catering_id",
     *         in="path",
     *         description="catering id",
     *         required=true,
     *         type="string",
     *     ),
     * @SWG\Parameter(
     *   name="catering_menu",
     *   in="body",
     *   description="catering menu object",
     *   required=true,
     *   @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="catering_menu",
     *         type="array",
     *         @SWG\Items(
     *           type="object",
     *           @SWG\Property(property="menu_id", type="string"),
     *           @SWG\Property(property="quantity", type="string")
     *         )
     *       )
     *   )
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addCateringMenu(Request $request,$id){


        // Perform Validation
        $validator = Validator::make($request->all(), [
            'catering_menu' => 'required|array',
            'catering_menu.0.menu_id' => 'required',
            'catering_menu.0.quantity' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        $catering = new Catering();
        $cateringData = $catering::find($id);
        if ($cateringData) {
            $array = $request->input('catering_menu');
            foreach($array as $arr){

                $cateringMenu = new CateringMenu();
                $cateringMenu->catering_id=$id;
                $cateringMenu->menu_id=$arr['menu_id'];
                $cateringMenu->quantity=$arr['quantity'];
                $cateringMenu->save();

            }
            $get = $cateringMenu::with(['cateringMenuItem.menuItemCategory'])->where('catering_id',$id)->get();
            return $this->response($get, 's', '200', 'catering menu has been added');
        } else {
            return $this->response(null, 'f', '404', 'catering id is invalid');
        }
    }
    /**
     * @SWG\Put(
     *     path="/catering/{catering_id}/menu",
     *     summary="This api is used edit catering menu",
     *     tags={"Restaurant App"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="catering_id",
     *         in="path",
     *         description="catering id",
     *         required=true,
     *         type="string",
     *     ),
     * @SWG\Parameter(
     *   name="catering_menu",
     *   in="body",
     *   description="catering menu object",
     *   required=true,
     *   @SWG\Schema(
     *       type="object",
     *       @SWG\Property(
     *         property="catering_menu",
     *         type="array",
     *         @SWG\Items(
     *           type="object",
     *           @SWG\Property(property="menu_id", type="string"),
     *           @SWG\Property(property="quantity", type="string")
     *       )
     *         )
     *       )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function editCateringMenu(Request $request,$id){
        // Perform Validation
        $validator = Validator::make($request->all(), [
            'catering_menu' => 'required|array',
            'catering_menu.0.menu_id' => 'required',
            'catering_menu.0.quantity' => 'required'
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }

        $catering = new Catering();
        $cateringData = $catering::find($id);
        if ($cateringData) {

            //remove old record first before adding new one
            $cateringMenu = new CateringMenu();
            $ids = $cateringMenu::all()->where('catering_id',$id)->pluck('id');
            if(count($ids) > 0){
                foreach($ids as $d){
                    $cateringMenu::find($d)->delete();
                }
            }

            $array = $request->input('catering_menu');
            foreach($array as $arr){

                $cateringMenu = new CateringMenu();
                $cateringMenu->catering_id=$id;
                $cateringMenu->menu_id=$arr['menu_id'];
                $cateringMenu->quantity=$arr['quantity'];
                $cateringMenu->save();

            }
            $get = $cateringMenu::with(['cateringMenuItem.menuItemCategory'])->where('catering_id',$id)->get();
            return $this->response($get, 's', '200', 'catering menu has been edited');
        } else {
            return $this->response(null, 'f', '404', 'catering id is invalid');
        }
    }
    /**
     * @SWG\Delete(
     *     path="/catering/{catering_id}/menu",
     *     summary="This api is used delete catering menu",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="catering_id",
     *         in="path",
     *         description="catering id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function deleteCateringMenu($id)
    {


        $cateringMenu = new CateringMenu();
        $ids = $cateringMenu::all()->where('catering_id',$id)->pluck('id');
        if(count($ids) > 0){
            foreach($ids as $d){
                $cateringMenu::find($d)->delete();
            }
        }

        return $this->response(null, 's', '200', 'item has been deleted');




    }



}
