<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;
use \File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\User;
use App\Models\Truck;
use App\Models\TruckArea;
use App\Models\TruckCategory;

class TruckController extends ApiController
{
    public function __construct(Request $request)
    {

        $this->request = $request;

    }

    /**
     * @SWG\Get(
     *     path="/trucks",
     *     summary="This api is used get truck list",
     *     tags={"User App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/trucks/{area_id}",
     *     summary="This api is used get truck list by area id",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="area_id",
     *         in="path",
     *         description="area_id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/trucks/filter/{category_id}",
     *     summary="This api is used get truck list by category id",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="category_id",
     *         in="path",
     *         description="category_id (ex. 1,2,3)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	 
	 /**
     * @SWG\Get(
     *     path="/trucks/{latitude}/{longitude}/{radius}",
     *     summary="This api is used get truck list by latitude, longitude and radius",
     *     tags={"User App"},
	 *     @SWG\Parameter(
     *         name="latitude",
     *         in="path",
     *         description="latitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="longitude",
     *         in="path",
     *         description="longitude",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="radius",
     *         in="path",
     *         description="radius",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getTrucks($latitude = '', $longitude = '', $radius = '', $area_id = '', $category_id = '')
    {
        $truck = new User();
        $detail = $truck->getTrucks($latitude, $longitude, $radius, $area_id, $category_id);

        if ($detail) {
            return $this->response($detail, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to get truck list');
    }
    /**
     * @SWG\Post(
     *     path="/truck",
     *     summary="This api is used to add or update truck detail of user",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="formData",
     *         description="user id of user who add truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="title of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sub_title",
     *         in="formData",
     *         description="sub title of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="description of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lat",
     *         in="formData",
     *         description="latitude of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lng",
     *         in="formData",
     *         description="longitude of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="block",
     *         in="formData",
     *         description="block number of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="street",
     *         in="formData",
     *         description="street of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="home_no",
     *         in="formData",
     *         description="home_no of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="city of truck",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="area",
     *         in="formData",
     *         description="area id of area (Ex: 1,2,3)",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="category",
     *         in="formData",
     *         description="category id of category (Ex: 1,2,3)",
     *         required=true,
     *         type="string"
     *     ),
	 *     @SWG\Parameter(
     *         name="is_active",
     *         in="formData",
     *         description="Active/Inactive of user(1 = active, 0 = inactive)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="main_image",
     *         in="formData",
     *         description="main image of truck",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function addTruck(Request $request)
    {

        // Perform Validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'title' => 'required',
            'sub_title' => 'required',
            'description' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'block' => 'required',
            'street' => 'required',
            'home_no' => 'required',
            'city' => 'required',
            'area' => 'required',
            'category' => 'required',
            'is_active' => 'required',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $user = $user->find($request->input('user_id'));
        if (!$user) {
            return $this->response(null, 'f', '500', 'user id is invalid');
        } else {
				$user->is_active = $request->input('is_active');
				$user->save();
				$request->request->remove('is_active');
		}
        $truck = new Truck();
        $get = $truck::where('user_id',$request->input('user_id'))->first();

        if(!$get){
            $validator = Validator::make($request->all(), [
                'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }
        }

        $image = $request->file('main_image');
        if ($image) {

            if($get){
                //remove old image if new image uploaded on update truck
                $deleteImageName = pathinfo($get->image)['basename'];
				if(file_exists('uploads/images/truck/original/'.$deleteImageName)){
					unlink('uploads/images/truck/original/'.$deleteImageName);
					unlink('uploads/images/truck/thumb/'.$deleteImageName);
				}
            }

            $validator = Validator::make($request->all(), [
                'main_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator->errors()->count()) {
                return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
            }
            $filename = $image->hashName();

            $originalPath = base_path('public/uploads/images/truck/original/'. $filename);
            $thumbPath = base_path('public/uploads/images/truck/thumb/' . $filename);
            $image_resize = Image::make($image->getRealPath());



            //save original file
            $image_resize->save($originalPath);

            $image_resize->resize(723, 332, function ($constraint) {
                //$constraint->aspectRatio();
            });

            //save resize file
            $imageSave = $image_resize->save($thumbPath);
            //set image url
            $imageUrl = url(Storage::disk('truck')->url($filename));

            if (!$imageSave)
                return $this->response(null, 'f', '500', 'failed to add truck image');

        } else {
            $imageUrl = $get->image;
        }
        $request->request->add(['image' => $imageUrl]);
		$areas = $request->input('area');
		$request->request->remove('area');
		$categories = $request->input('category');
		$request->request->remove('category');
        if($get){
			$data = $request->except('main_image');
            $save = $get->update($data);
            $save = $truck::where('user_id',$request->input('user_id'))->first();
        }else{
            $save = $truck->create($request->all());
        }
		
		if ($areas) {
			$truckareas = explode(',', $areas);
			TruckArea::where('truck_id', $save->id)->delete();
			foreach($truckareas as $area){
				$TruckArea = new TruckArea();
				$TruckArea->area_id = $area;
				$save->truckarea()->save($TruckArea);
			}
		}
		
		if ($categories) {
			$truckcategories = explode(',', $categories);
			TruckCategory::where('truck_id', $save->id)->delete();
			foreach($truckcategories as $category){
				$TruckCategory = new TruckCategory();
				$TruckCategory->category_id = $category;
				$save->truckcategory()->save($TruckCategory);
			}
		}

        if ($save) {
            return $this->response($save, 's', '200', 'truck has been added successfully');
        }
        return $this->response(null, 'f', '500', 'failed to add truck detail');


    }
	
	 /**
     * @SWG\Post(
     *     path="/updatetruck",
     *     summary="This api is update truck detail ie. latitude, longitude, block, street, home_no",
     *     tags={"Restaurant App"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="formData",
     *         description="user id of user who add truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lat",
     *         in="formData",
     *         description="latitude of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lng",
     *         in="formData",
     *         description="longitude of truck",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="block",
     *         in="formData",
     *         description="block number of truck",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="street",
     *         in="formData",
     *         description="street of truck",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="home_no",
     *         in="formData",
     *         description="home_no of truck",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="city of truck",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
	
	public function updateTruck(Request $request)
    {
        // Perform Validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($validator->errors()->count()) {
            return $this->response(null, 'f', '422', 'The given data was invalid.', $validator->errors());
        }
        $user = new User();
        $user = $user->find($request->input('user_id'));
        if (!$user) {
            return $this->response(null, 'f', '500', 'user id is invalid');
        }
        $truck = new Truck();
        $get = $truck::where('user_id',$request->input('user_id'))->first();

        
        if($get){
            $save = $truck::where('user_id',$request->input('user_id'))->update($request->all());
            $save = $truck::where('user_id',$request->input('user_id'))->first();
        }

        if ($save) {
            return $this->response($save, 's', '200', 'truck has been updated successfully');
        }
        return $this->response(null, 'f', '500', 'failed to update truck detail');
    }
	
    /**
     * @SWG\Get(
     *     path="/user/{user_id}/truck-catering",
     *     summary="This api is used get truck and catering detail of user",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getTruck($id)
    {
        $user = new User();
        $get = $user->find($id);
        if (!$get) {
            return $this->response(null, 'f', '500', 'user not found');
        }

        $detail = $user->getTruck($id);
        if ($detail) {
            return $this->response($detail, 's', '200', '');
        } else {
            return $this->response(new Object_(), 's', '200', 'no truck detail found for this user');
        }


    }
	/**
     * @SWG\Get(
     *     path="/truck/{user_id}",
     *     summary="This api is used get truck detail",
     *     tags={"Restaurant App"},
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getTruckDetail($id)
    {
        $user = new User();
        $get = $user->find($id);
        if (!$get) {
            return $this->response(null, 'f', '500', 'user not found');
        }

        $detail = Truck::with('truckarea')->where('user_id', $id)->first();
        if ($detail) {
            return $this->response($detail, 's', '200', '');
        } else {
            return $this->response(new Object_(), 's', '200', 'no truck detail found for this user');
        }
    }
}
