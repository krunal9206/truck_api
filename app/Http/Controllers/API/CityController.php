<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\City;


class CityController extends ApiController
{
    /**
     * @SWG\Get(
     *     path="/areas",
     *     summary="This api is used to get area list",
     *     tags={"Restaurant App"},
     *     @SWG\Response(
     *         response=200,
     *         description="success"
     *     )
     * )
     */
    public function getAreas()
    {
        $areas = City::with('areas')->get();

        if ($areas) {
            return $this->response($areas, 's', '200', '');
        }
        return $this->response(null, 'f', '500', 'failed to get area list');
    }
}
