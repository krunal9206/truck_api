<?php

namespace App\Http\Controllers;
//use Davibennun\LaravelPushNotification\Facades\PushNotification;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

/**
 * @SWG\Swagger(
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Truck API",
 *         contact={"email":"krunal9206@gmail.com"},
 *     ),
 *     consumes={"application/x-www-form-urlencoded"},
 *     produces={"application/json"}
 * )
 */
/**
 * @SWG\Tag(
 *   name="User App",
 *   description="User App APIs",
 * )
 *  @SWG\Tag(
 *   name="Driver App",
 *   description="Driver App APIs",
 * )
 * @SWG\Tag(
 *   name="Restaurant App",
 *   description="Restaurant App APIs",
 * )
 */
class ApiController extends Controller
{


    public function response($data,$status='fail',$code='200',$message='',$trace=''){

        if($status == 's'){
            $status = 'success';
        }else if($status == 'f'){
            $status = 'fail';
        }
        $res=[];
        $res['status']=$status;
        $res['code']=$code;
        $res['data']=$data;
        $res['message']=$message;

        if($status == 'fail'){
            $res['trace']=$trace;
        }


        return Response()->json($res,$code);
    }
    public function testPushNotification(){

        $device=isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : "";
        if(empty($device)){
            echo "device_id must be given";exit;
        }

        try{


            $message = 'Testing Push';
            $data = ['xyz' => 'abc','message'=>$message,'screen'=>'order'];
            $response =  $this->sendPushNotification($device, '', $message, $data, 'XYZ Screen');
           if($response->numberSuccess())
               echo "Sent";
           else if($response->numberFailure())
               echo "Could not Sent";
           echo "<br /><br />log here <br />";

           echo "<pre>";
           print_r($response);

        }catch (\Exception $e){
            echo $e->getMessage();
        }

    }
    public function sendPushNotification($token, $title = 'Truck', $body, $payload, $clickAction = Null)
    {

        if (!$token) {
             return;
        }

        /*$body = array('message'=>$body,'data'=>$payload);
        $body = ['message'=>'asd','data'=>$payload];*/


        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60 * 20);
        $option = $optionBuiler->build();

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder
            ->setBody($body)
            ->setSound('default')
            ->setClickAction($clickAction);

        $notification = $notificationBuilder->build();

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($payload); // data Payload
        $data = $dataBuilder->build();

        //$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();

        return $downstreamResponse;
    }
}
