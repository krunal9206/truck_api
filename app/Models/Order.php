<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\Collection;

class Order extends Model {

    protected $table = 'orders';
    protected $fillable = [
        'status'
    ];
    protected $hidden = ['user_id','coupon_id','address_id','created_at','updated_at'];
    public function cart() {
        return $this->hasmany( 'App\Models\Cart', 'order_id');
    }
    public function getOrders($id,$type){

        //DB::enableQueryLog();
        $new = new Collection();

        if($type == 'user'){
            $carts = $this::with(['cart.attribute'])->where('user_id',$id)->orderBy('id', 'desc')->get();
			foreach($carts as $cart) {
				if($cart->order_type == 'Catering'){
					$catering = DB::table('catering')->where('id', $cart->restaurant_id)->select('user_id')->first();
					$truck = DB::table('truck')->where('user_id', $catering->user_id)->select('lat', 'lng')->first();
					$cart->truck = array(
						'lat' => (float)$truck->lat,
						'lng' => (float)$truck->lng,
					);
				}
				$new->push($cart);
			}
        } else if($type == 'restaurant'){

            $truck = new Truck();
            $truckData =  $truck::where('user_id', $id)->select('id')->first();

            $cart = $this::with(['cart.attribute'])->where('restaurant_id',$truckData->id)->get();
            $new->push($cart);
            /*foreach($carts as $cart) {

                if(!empty($cart->cart) && count($cart->cart) > 0){
                    foreach($cart->cart as $c) {
                        if(!empty($c->truck) && count($c->truck) > 0){
                            unset($c->truck);
                            if(!$new->contains('id',$cart->id))
                                $new->push($cart);
                        }

                    }
                }

            }*/
            /*$carts = $this::with(['cart.truck'=> function ($query) use ($id){
                $query->where('user_id', '=', $id);
            }])->get();

            foreach($carts as $cart) {

                if(!empty($cart->cart) && count($cart->cart) > 0){
                    foreach($cart->cart as $c) {
                        if(!empty($c->truck) && count($c->truck) > 0){
                            unset($c->truck);
                            if(!$new->contains('id',$cart->id))
                            $new->push($cart);
                        }

                    }
                }

            }*/
        }

       /*dd(
            DB::getQueryLog()
        );*/
        return $new;
        //return $new->unique();

    }

}