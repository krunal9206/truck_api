<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {
	
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'image',
		'offer_percentage',
		'is_active',
		'is_catering'
    ];
    protected $hidden = ['user_id' ,'updated_at', 'created_at'];
	
}