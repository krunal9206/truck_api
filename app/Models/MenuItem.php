<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MenuItem extends Model {

    protected $table = 'menu_item';
    protected $fillable = [
        'category_id',
        'title',
        'sub_title',
        'image',
        'price'
    ];
    protected $hidden = ['category_id','status','updated_at', 'created_at'];
    public function menuItemAttribute() {
        return $this->hasMany( 'App\Models\MenuItemAttribute','menu_item_id');
    }
    public function menuItemCategory() {
        return $this->hasOne( 'App\Models\MenuCategory','id','category_id');
    }
}