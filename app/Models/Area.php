<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	
	protected $fillable = [
        'city_id',
        'name',
    ];
	
	protected $hidden = ['city_id', 'created_at', 'updated_at'];
	
	public function city(){
        return $this->belongsTo('App\Models\City');
    }
}
