<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Cart extends Model {

    protected $table = 'cart';
    protected $hidden = ['order_id','truck_id','catering_id','menu_id','menu_attribute_id','created_at','updated_at'];
    public function truck() {
        return $this->hasOne( 'App\Models\Truck','id','restaurant_id');
    }
    public function attribute() {
        return $this->hasMany( 'App\Models\CartAttribute','cart_id');
    }


}