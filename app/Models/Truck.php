<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Truck extends Model {

    protected $table = 'truck';
    protected $fillable = [
        'user_id',
        'title',
        'sub_title',
        'image',
        'description',
        'lat',
        'lng',
        'block',
        'street',
        'home_no',
        'city',
    ];
    protected $hidden = ['updated_at', 'created_at'];

    public function menuCategory() {
        return $this->hasmany( 'App\Models\MenuCategory','truck_id');
    }
    public function catering() {
        return $this->hasMany( 'App\Models\Catering','user_id','user_id');
    }
    public function truckReview() {
        return $this->hasMany( 'App\Models\Review','type_id')->where('type','Truck');
    }
    public function cateringReview() {
        return $this->hasMany( 'App\Models\Review','type_id')->where('type','Catering');
    }
    public function getOwner($id){
       return self::where('id', $id)->select('user_id')->first();
    }
	public function offer() {
        return $this->hasOne( 'App\Models\Offer','user_id','user_id');
    }
	public function truckarea() {
        return $this->hasmany('App\Models\TruckArea');
    }
	public function truckcategory() {
        return $this->hasmany('App\Models\TruckCategory');
    }
}