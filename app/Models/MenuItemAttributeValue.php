<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MenuItemAttributeValue extends Model {

    protected $table = 'menu_item_attribute_value';
    protected $hidden = ['attribute_id','status','updated_at', 'created_at'];

}