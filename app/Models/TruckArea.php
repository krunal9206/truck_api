<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TruckArea extends Model
{
    protected $fillable = [
        'truck_id',
        'area_id',
    ];
    protected $hidden = ['truck_id', 'created_at', 'updated_at'];
}
