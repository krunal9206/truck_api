<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\Collection;

class User extends Model {

    protected $table = 'user';
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'password',
        'user_type',
        'mobile_verify_code',
        'email_verify_code',
        'reset_password_verify_code',
        'is_login',
        'status',
        'device_token',
		'is_active'
    ];
    protected $hidden = [ 'mobile','user_type','mobile_verify_code','email_verify_code','reset_password_verify_code','status','password','updated_at','created_at','is_login','device_token'];

    public function truck() {
        return $this->hasOne( 'App\Models\Truck');
    }
    public function getTruck($id){
        $trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.catering.cateringMenu.cateringMenuItem.menuItemCategory', 'truck.offer'])->where('id',$id)->get();

        $new = new Collection();
        foreach($trucks as $truck) {

            if(!empty($truck->truck))
                $new->push($truck->truck);
        }
        return $new->first();
    }
    public function getTrucks($latitude, $longitude, $radius, $area_id, $category_id){
        //DB::enableQueryLog();
		if($latitude && $longitude && $radius) {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')
			->where('is_active',1)->whereHas('truck', function($q) use ($latitude, $longitude, $radius){
				$q->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$latitude, $longitude, $latitude])
				->havingRaw("distance <= ?", [$radius]);
			})
			->get();
		} else if($area_id) {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')
			->where('is_active',1)->whereHas('truck.truckarea', function($q) use ($area_id){
				$q->where('area_id', $area_id);
			})->get();
		} else if($category_id) {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')
			->where('is_active',1)->whereHas('truck.truckcategory', function($q) use ($category_id){
				$q->whereIn('category_id', explode(',', urldecode($category_id)));
			})->get();
		} else {
$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->get();
		}

        // Store or dump the log data...
        /* dd(
            DB::getQueryLog()
        ); */
        $new = new Collection();
        foreach($trucks as $truck) {


            if(!empty($truck->truck)){
                $truckId = $truck->truck->id;
                if(!empty($truck->truck->menuCategory) && count($truck->truck->menuCategory) > 0){

                    foreach($truck->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }

                $new->push($truck->truck);
            }

        }
        return $new;
    }
    public function getCaterings($latitude, $longitude, $radius, $area_id, $category_id){
        //DB::enableQueryLog();
		if($latitude && $longitude && $radius) {
			$caterings = $this::with(['truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)
			->whereHas('truck', function($q) use ($latitude, $longitude, $radius){
				$q->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$latitude, $longitude, $latitude])
				->havingRaw("distance <= ?", [$radius]);
			})
			->get();
		} else if($area_id) {
			$caterings = $this::with(['truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->whereHas('truck.truckarea', function($q) use ($area_id){
				$q->where('area_id', $area_id);
			})->get();
		} else if($category_id) {
			$caterings = $this::with(['truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->whereHas('truck.truckcategory', function($q) use ($category_id){
				$q->whereIn('category_id', explode(',', urldecode($category_id)));
			})->get();
		} else {
			$caterings = $this::with(['truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->get();
		}
        /*dd(
            DB::getQueryLog()
        );*/
        $new = new Collection();
        foreach($caterings as $catering) {
            /*if(!empty($catering->truck) && count($catering->truck->catering) > 0)
                $new->push($catering->truck);*/

            if(!empty($catering->truck) && count($catering->truck->catering) > 0){
                $truckId = $catering->truck->id;

                if(!empty($catering->truck->catering) && count($catering->truck->catering) > 0){

                    foreach($catering->truck->catering as $menuCat) {
                        $menuCat->restaurant_id=$truckId;

                    }
                }
                if(!empty($catering->truck->menuCategory) && count($catering->truck->menuCategory) > 0){

                    foreach($catering->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }

                $new->push($catering->truck);
            }
        }

        return $new;

    }
	
	public function getOffers($latitude, $longitude, $radius){
        //DB::enableQueryLog();
		if($latitude && $longitude && $radius) {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)
			->whereHas('truck', function($q) use ($latitude, $longitude, $radius){
				$q->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$latitude, $longitude, $latitude])
				->havingRaw("distance <= ?", [$radius]);
			})
			->whereHas('truck.offer', function($q){
				$q->where('is_catering', 0);
			})
			->get();
		} else {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->whereHas('truck.offer', function($q){
				$q->where('is_catering', 0);
			})->get();
		}
        /*dd(
            DB::getQueryLog()
        );*/
        $new = new Collection();
		
		foreach($trucks as $truck) {


            if(!empty($truck->truck)){
                $truckId = $truck->truck->id;
                if(!empty($truck->truck->menuCategory) && count($truck->truck->menuCategory) > 0){

                    foreach($truck->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }
				
				if(!empty($catering->truck->catering) && count($catering->truck->catering) > 0){

                    foreach($catering->truck->catering as $menuCat) {
                        $menuCat->restaurant_id=$truckId;

                    }
                }
                if(!empty($catering->truck->menuCategory) && count($catering->truck->menuCategory) > 0){

                    foreach($catering->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }
				if($truck->truck->offer){
					$new->push($truck->truck);
				}
            }

        }
		
        return $new;
    }
	
	
	public function getOffersCatering($latitude, $longitude, $radius){
        //DB::enableQueryLog();
		if($latitude && $longitude && $radius) {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)
			->whereHas('truck', function($q) use ($latitude, $longitude, $radius){
				$q->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$latitude, $longitude, $latitude])
				->havingRaw("distance <= ?", [$radius]);
			})
			->whereHas('truck.offer', function($q){
				$q->where('is_catering', 1);
			})
			->get();
		} else {
			$trucks = $this::with(['truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.truckReview.user', 'truck.catering.cateringMenu.cateringMenuItem.menuItemCategory','truck.menuCategory.menuItem.menuItemAttribute.menuItemAttributeValue','truck.cateringReview.user', 'truck.offer'])->where('user_type','restaurant')->where('status','active')->where('is_active',1)->whereHas('truck.offer', function($q){
				$q->where('is_catering', 1);
			})->get();
		}
        /*dd(
            DB::getQueryLog()
        );*/
        $new = new Collection();
		
		foreach($trucks as $truck) {


            if(!empty($truck->truck)){
                $truckId = $truck->truck->id;
                if(!empty($truck->truck->menuCategory) && count($truck->truck->menuCategory) > 0){

                    foreach($truck->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }
				
				if(!empty($catering->truck->catering) && count($catering->truck->catering) > 0){

                    foreach($catering->truck->catering as $menuCat) {
                        $menuCat->restaurant_id=$truckId;

                    }
                }
                if(!empty($catering->truck->menuCategory) && count($catering->truck->menuCategory) > 0){

                    foreach($catering->truck->menuCategory as $menuCat) {
                        if(!empty($menuCat->menuItem) && count($menuCat->menuItem) > 0){
                            foreach($menuCat->menuItem as $menuItem) {
                                $menuItem->restaurant_id=$truckId;

                            }
                        }
                    }
                }
				if($truck->truck->offer){
					$new->push($truck->truck);
				}
            }

        }
		
        return $new;
    }
	
    public function getPushData($id){
        //return $this::find($id)->toArray();
        //return $this::find($id)->toArray();
        $user = self::where('id', $id)->where('is_login','yes')->select('device_token')->first();
        return $user;
    }
    public function createUser($input){
        return $this->create($input->all());
    }
    public function updateUser($id,$input){

        $updated = $this->find($id)->update($input);
        $user = $this->find($id);
        if($updated) {
            return $user;
        }
        return false;
    }
}