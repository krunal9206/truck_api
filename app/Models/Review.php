<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Review extends Model {

    protected $table = 'review';
    protected $fillable = [
        'user_id',
        'type',
        'type_id',
        'star',
        'comment'
    ];
    protected $hidden = ['updated_at'];
    public function user() {
        return $this->hasOne( 'App\Models\User','id','user_id');
    }

}