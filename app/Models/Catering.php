<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Catering extends Model {

    protected $table = 'catering';
    protected $fillable = [
        'user_id',
        'title',
        'sub_title',
        'image',
        'price',
        'serve_people',
		'setup_time',
		'max_time',
		'service_presentation'
    ];
    protected $hidden = ['user_id','status','updated_at','created_at'];

    public function cateringMenu() {
        return $this->hasmany( 'App\Models\CateringMenu','catering_id');
    }


}