<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MenuItemAttribute extends Model {

    protected $table = 'menu_item_attribute';
    protected $hidden = ['menu_item_id','status','updated_at', 'created_at'];
    public function menuItemAttributeValue() {
        return $this->hasMany( 'App\Models\MenuItemAttributeValue','attribute_id');
    }
}