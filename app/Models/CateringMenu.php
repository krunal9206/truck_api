<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class CateringMenu extends Model {

    protected $table = 'catering_menu';
    protected $fillable = [
        'catering_id',
        'menu_id',
        'quantity'
    ];
    protected $hidden = ['catering_id','menu_id','status','updated_at','created_at'];

    public function cateringMenuItem() {
        return $this->hasOne( 'App\Models\MenuItem','id','menu_id');
    }



}