<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MenuCategory extends Model {

    protected $table = 'menu_category';
    protected $fillable = [
        'truck_id',
        'parent_category_id',
        'title',
        'sub_title',
        'image'
    ];
    protected $hidden = ['parent_category_id','truck_id','status','updated_at', 'created_at'];
    public function menuItem() {
        return $this->hasMany( 'App\Models\MenuItem','category_id');
    }
}