<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TruckCategory extends Model
{
     protected $fillable = [
        'truck_id',
        'category_id',
    ];
    protected $hidden = ['truck_id', 'created_at', 'updated_at'];
}
