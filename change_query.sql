ALTER TABLE `catering` ADD `setup_time` VARCHAR(255) NOT NULL AFTER `serve_people`, ADD `max_time` VARCHAR(255) NOT NULL AFTER `setup_time`, ADD `service_presentation` VARCHAR(255) NOT NULL AFTER `max_time`;

ALTER TABLE `offers` ADD `is_active` TINYINT(1) NOT NULL AFTER `offer_percentage`;
ALTER TABLE `offers` ADD `is_catering` TINYINT(1) NOT NULL AFTER `is_active`;

ALTER TABLE `orders` CHANGE `payment_type` `payment_type` ENUM('Cash On Delivery','Credit Card','Pick up order') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;